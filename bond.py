__author__ = 'Zengye'
import string
import math
from datetime import date

if __name__ == "__main__":
    a=date(2012,10,1)
    b=date(2013,10,1)
    c = b-a
    print (b-a).days/365.0

class yield_curve(object):
    def __init__(self, data):
        self.yc_list = []
        lines = data.split('\n')
        for cnt in range(1, len(lines)):  # ignore the header line
            line = lines[cnt].split(',')
            self.yc_list.append( (float(line[0]), float(line[1])/100.0) )

        #print self.yc_list

    def get_yield(self, delta_time):
        # date's day must be the first day of that month
        # assert date.day == 1
        # delta time in years
        # delta_time = (date - today).days/365.0
        # using binary search to find the yield
        lower = 0
        upper = len(self.yc_list)-1
        target_idx = -1
        while lower<upper-1:   # when exist this loop, lower = upper-1
            idx = (lower+upper)/2 #idx is an int
            if self.yc_list[idx][0] < delta_time:
                lower = idx
            elif self.yc_list[idx][0] > delta_time:
                upper = idx
            else:
                target_idx = idx
        # found the node on the yc curve
        if target_idx != -1:
            return self.yc_list[target_idx][1]
        # else, using linear interpolation
        t1 = self.yc_list[lower][0]
        y1 = self.yc_list[lower][1]
        t2 = self.yc_list[upper][0]
        y2 = self.yc_list[upper][1]
        _yield = y1 + (y2-y1)/(t2-t1)*(delta_time-t1)
        return _yield

class bond(object):
    def __init__(self, id, type, notional, maturity, rate, compounding, today):
        self.id = int(id)
        self.type = type
        self.notional = float(notional)  # float
        # datetime.date
        self.maturity = date( int(maturity[0:4]), int(maturity[4:6]), int(maturity[6:8]) )
        self.rate = float(rate)/100.0  # float
        self.compounding = int(compounding)
        self.today = today

        self.yield_curve = ""
        self.coupon_time = []
        self.interest_rates = []
        self.payments = []

    def set_yield_curve(self, yc):
        self.yield_curve = yc
        if self.type == "FIXED":
            self.cal_coupon_time()
            # print self.coupon_time
            self.cal_interest_rates()
            # print self.interest_rates
            self.cal_payment()
            # print self.payments

    def get_parameters(self):
        if self.type=="FIXED":
            self.cal_price()
            self.cal_par_yield()
            self.cal_duration_and_convexity()
            return "%d,%f,%f,%f,%f\n" % (self.id, self.bond_price, self.par_yield, self.duration, self.convexity)
        elif self.type=="FLOAT":
            next_payment_time = self.get_next_payment_time()
            zero_rate = self.yield_curve.get_yield(next_payment_time)
            next_payment = self.notional*(self.rate/self.compounding + 1)

            #next_payment = self.notional*math.pow(1+self.rate/self.compounding, self.compounding*next_payment_time)
            B = next_payment*math.pow(1+zero_rate/2.0, -2*next_payment_time) #math.exp(-next_payment_time*rate)
            tmp = next_payment/self.notional
            tmp2 = math.exp(math.log(tmp)/(2*next_payment_time))
            y = (tmp2 - 1)*2
            return "%d,%f,%f,%f,%f\n" % (self.id, B, y, next_payment_time, next_payment_time*next_payment_time)

    def cal_price(self):
        if self.yield_curve == "":
            print "must assign yield curve first! "
            exit(-1)
        assert len(self.coupon_time) == len(self.interest_rates) == len(self.payments)
        B = 0
        for i in range(len(self.coupon_time)):
            ti = self.coupon_time[i]
            ri = self.interest_rates[i]
            pi = self.payments[i]
            B += pi* math.pow(1+ri/2.0, -ti*2) #math.exp(-ri*ti)
        self.bond_price = B
        # 2 ways to calculate a floater
        if self.type == "FLOAT":
            '''import matplotlib.pyplot as plt
            plt.plot(self.coupon_time, self.interest_rates)
            plt.show()
            print self.coupon_time
            print self.interest_rates
            print self.payments'''
            t0 = self.coupon_time[0]
            r0 = self.interest_rates[0]
            p0 = self.payments[0] + self.notional
            B2 = p0* math.pow(1+r0/2.0, -t0*2) # math.exp(-t0*r0)
            print "B2 - B1 is %f"% (B2-B)

    def cal_par_yield(self):
        if self.bond_price ==0:
            self.cal_price()

        print self.coupon_time
        print self.payments

        N = len(self.coupon_time)
        # using newton's method to calculate yield
        x0 = 0.1
        x_new = x0
        x_old = x0 - 1
        while abs(x_new-x_old) > math.pow(10, -6):
            x_old = x_new
            sum1 = 0
            sum2 = 0
            for i in range(N):
                ti = self.coupon_time[i]
                coef = math.pow(1+x_old/2.0, -ti*2) #math.exp(-x_old*ti)
                pi = self.payments[i]
                sum1 += pi*coef
                sum2 += pi*ti*coef
            x_new = x_old + (sum1-self.notional)/sum2
        self.par_yield = x_new
        # test par yield
        '''
        tmp = []
        B = 0
        y = self.par_yield
        for i in range(N):
            pi = self.payments[i]
            ti = self.coupon_time[i]

            B += pi*math.exp(-y*ti)
            tmp.append(pi*math.exp(-y*ti))
        print tmp
        print "self.B - B is %f" % (self.bond_price - B)
        i=1'''

    def cal_duration_and_convexity(self):
        y = self.par_yield
        N = len(self.coupon_time)
        sum_1t = 0
        sum_2t = 0
        for i in range(N):
            pi = self.payments[i]
            ti = self.coupon_time[i]
            tmp = pi*ti* math.pow(1+y/2.0, -2*ti) #math.exp(-y*ti)
            sum_1t += tmp
            sum_2t += tmp*ti
        self.duration = sum_1t/self.bond_price
        self.convexity = sum_2t/self.bond_price

    def cal_coupon_time(self):
        # minus 3 months each step
        end = self.maturity
        step = 12/self.compounding  # compounding must divide 12
        while end >= self.today:
            self.coupon_time.append((end-self.today).days/365.0)
            month = end.month - step
            year = end.year
            if month <= 0:
                month += 12
                year -= 1
            end = date(year, month, 1)
        '''
        #ttm: time_to_maturity, in years
        ttm = (self.maturity - self.today).days/365.0
        step = 1.0/self.compounding
        ticker = ttm
        while ticker > 0 :
            self.coupon_time.append(ticker)
            ticker -= step
        '''
        #make sure time in in ascending order
        self.coupon_time.reverse()
    def get_next_payment_time(self):
        if self.type == "FLOAT":
            end = self.maturity
            step = 12/self.compounding  # compounding must divide 12
            while end >= self.today:
                result = (end-self.today).days/365.0
                month = end.month - step
                year = end.year
                if month <= 0:
                    month += 12
                    year -= 1
                end = date(year, month, 1)
            return result

    def cal_interest_rates(self):
        # get interest rate corresponding to the coupon time
        for i in range(len(self.coupon_time)):
            rate = self.yield_curve.get_yield(self.coupon_time[i])
            #rate = yield_curve.get_yield(self.yield_curve, self.coupon_time[i])
            self.interest_rates.append(rate)
        # get payments corresponding to the coupon time

    def cal_payment(self):
        if self.type == "FIXED":
            coupon = self.notional*self.rate/self.compounding
            for i in range(len(self.coupon_time)):
                self.payments.append(coupon)
            self.payments[len(self.payments)-1] += self.notional
            # print self.notional, self.payments
        elif self.type == "FLOAT":
            # add the first coupon
            coupon = self.notional*self.rate/self.compounding
            self.payments.append(coupon)
            for i in range(1, len(self.interest_rates)):
                r2 = self.interest_rates[i]
                r1 = self.interest_rates[i-1]
                t2 = self.coupon_time[i]
                t1 = self.coupon_time[i-1]
                # forward rate
                f_rate = (r2*t2 - r1*t1)/(t2-t1)
                coupon = self.notional*f_rate/self.compounding
                self.payments.append(coupon)
            self.payments[len(self.payments)-1] += self.notional
            # print self.notional, self.payments

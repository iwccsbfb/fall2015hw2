"""
Project: Homework 2-Question 2-MTH 9815
Author: Zengye Wang, Chi Ma, Jiabei Ma
Date: Oct 31, 2015
"""


# Python imports
import sys
# 3rd party imports
import numpy as np
import pandas as pd


# Input command line arguments
argList = sys.argv
f_principal = float(sys.argv[1])
f_interestRate = float(sys.argv[2]) / 100.
f_extraPayment = float(sys.argv[3])


def monthly_payment(f_principal, interest_rate_payment, years, extra_pmt=0.):
    # Payterm is the number of month for 15 or 30 years
    global Payterm
    if years == 15:
        Payterm = 12 * 15
    elif years == 30:
        Payterm = 12 * 30

    # pmt is the amount of monthly payment of 15 or 30 years
    pmt = f_principal / float((1 - pow((1 + interest_rate_payment / 12.), -Payterm)) / (interest_rate_payment / 12.)) + extra_pmt
    return pmt


def summary_info_mortgage(years):
    # set up a table named data, we will save result in this table
    global mortgage_data
    if years == 15:
        mortgage_data = np.zeros((180, 11))
    elif years == 30:
        mortgage_data = np.zeros((360, 11))

    # change the interest rate
    global interest_rate
    if years == 15:
        interest_rate = f_interestRate / 1.2
    elif years == 30:
        interest_rate = f_interestRate

    # calculate the first series of the table (0 point)
    payment = monthly_payment(f_principal, interest_rate, years, f_extraPayment)
    beginning_balance = f_principal
    interest = beginning_balance * (interest_rate / 12.)
    principal = payment - interest + f_extraPayment
    ending_balance = beginning_balance - principal

    # calculate the first series of the table (1 point)
    payment_point = monthly_payment(f_principal, interest_rate - 0.0025, years, f_extraPayment)
    beginning_balance_point = f_principal
    interest_point = beginning_balance_point * ((interest_rate - 0.0025) / 12.)
    principal_point = payment_point - interest_point + f_extraPayment
    ending_balance_point = beginning_balance_point - principal_point

    # input results in the table named data
    mortgage_data[0, 0] = beginning_balance
    mortgage_data[0, 1] = payment
    mortgage_data[0, 2] = interest
    mortgage_data[0, 3] = principal
    mortgage_data[0, 4] = ending_balance

    # using 'None' to separate two sub-tables
    mortgage_data[0, 5] = None

    mortgage_data[0, 6] = beginning_balance_point
    mortgage_data[0, 7] = payment_point
    mortgage_data[0, 8] = interest_point
    mortgage_data[0, 9] = principal_point
    mortgage_data[0, 10] = ending_balance_point

    # calculating the rest series of the table
    global payment_index
    if years == 15:
        payment_index = 180
    elif years == 30:
        payment_index = 360

    for pmt_num in xrange(1, payment_index, 1):
        # calculating the rest series of 0 point
        beginning_balance = ending_balance
        interest = beginning_balance * (interest_rate / 12.)
        principal = payment - interest + f_extraPayment
        ending_balance = beginning_balance - principal

        # input results in the table named data
        mortgage_data[pmt_num, 0] = beginning_balance
        mortgage_data[pmt_num, 1] = payment
        mortgage_data[pmt_num, 2] = interest
        mortgage_data[pmt_num, 3] = principal
        mortgage_data[pmt_num, 4] = ending_balance

        # using 'None' to separate two sub-tables
        mortgage_data[pmt_num, 5] = None

        # calculating the rest series of 1 point
        beginning_balance_point = ending_balance_point
        interest_point = beginning_balance_point * ((interest_rate - 0.0025) / 12.)
        principal_point = payment_point - interest_point + f_extraPayment
        ending_balance_point = beginning_balance_point - principal_point

        # input results in the table named data
        mortgage_data[pmt_num, 6] = beginning_balance_point
        mortgage_data[pmt_num, 7] = payment_point
        mortgage_data[pmt_num, 8] = interest_point
        mortgage_data[pmt_num, 9] = principal_point
        mortgage_data[pmt_num, 10] = ending_balance_point

    mortgage_summary = pd.DataFrame(mortgage_data)
    # save results to a text file
    if years == 15:
        np.savetxt("mortgage of 15 years.txt", mortgage_summary, delimiter='\t', fmt='{: ^10}'.format('%.2f'),
                   header="\t\tThe Monthly Payments of 15 Years Mortgage (0 point)"
                          "\t\t\t\t\t\tThe Monthly Payments of 15 Years Mortgage (1 point)",
                   comments="Comments:\tTwo sub-table are separated by 'nan'"
                            "\nThe 1st column is Beginning Balance (0 point)."
                            "\t\tThe 6th column is Beginning Balance (1 point)."
                            "\nThe 2nd column is Monthly Payment (0 point)."
                            "\t\tThe 7th column is Monthly Payment (1 point)"
                            "\nThe 3nd column is Interest (0 point)."
                            "\t\t\tThe 8nd column is Interest (1 point)."
                            "\nThe 4th column is Principal (0 point)."
                            "\t\t\tThe 9th column is Principal (1 point)."
                            "\nThe 5th column is ending Balance (0 point)."
                            "\t\tThe 10th column is ending Balance (1 point).\n\n\n")
    elif years == 30:
        np.savetxt("mortgage of 30 years.txt", mortgage_summary, delimiter='\t', fmt='{: ^10}'.format('%.2f'),
                   header="\t\tThe Monthly Payments of 30 Years Mortgage (0 point)"
                          "\t\t\t\t\t\tThe Monthly Payments of 30 Years Mortgage (1 point)",
                   comments="Comments:\tTwo sub-table are separated by 'nan'"
                            "\nThe 1st column is Beginning Balance (0 point)."
                            "\t\tThe 6th column is Beginning Balance (1 point)."
                            "\nThe 2nd column is Monthly Payment (0 point)."
                            "\t\tThe 7th column is Monthly Payment (1 point)"
                            "\nThe 3nd column is Interest (0 point)."
                            "\t\t\tThe 8nd column is Interest (1 point)."
                            "\nThe 4th column is Principal (0 point)."
                            "\t\t\tThe 9th column is Principal (1 point)."
                            "\nThe 5th column is ending Balance (0 point)."
                            "\t\tThe 10th column is ending Balance (1 point).\n\n\n")
    print mortgage_data

if __name__ == "__main__":
    summary_info_mortgage(15)
    summary_info_mortgage(30)
__author__ = 'Zengye'
import sys
import os
import string
import numpy as np
import pandas as pd
import Quandl
QUANDL_KEY = "yosxhWKxWGRMVkHoHq_k"

#-i symbols.csv -o minvarport.csv

def main():
    i = 1
    while i < len(sys.argv):
        if sys.argv[i] == "-i":
            i += 1
            i_file = sys.argv[i]
        elif sys.argv[i] == "-o":
            i += 1
            o_file = sys.argv[i]
        i += 1
    with open(i_file, 'r') as f:
        i_file_data = f.read()
    i_list = i_file_data.split('\n')
    i_list.remove(i_list[0])  # remove header

    '''
    i_list = ["AAPL","JPM","GOOG"]
    quandl_header = []
    for i in range(len(i_list)):
        quandl_header.append('WIKI/'+i_list[i]+'.4')
    aapl_data = Quandl.get(quandl_header, trim_start="2015-08-09", trim_end="2015-09-08", authtoken=QUANDL_KEY)
    '''

    X = pd.DataFrame()
    #miu = []
    for i in range(len(i_list)):
        line = i_list[i]
        aapl_data = Quandl.get("WIKI/"+line, trim_start="2014-11-01", trim_end="2015-10-31", authtoken=QUANDL_KEY)
        close_price =  aapl_data['Adj. Close']
        #print line, close_price.shape
        percentage_return = []
        for j in range(1, len(close_price)):
            percentage_return.append(close_price[j]/close_price[j-1])
        mean = sum(percentage_return)/len(percentage_return)
        for j in range(len(percentage_return)):
            percentage_return[j] -= mean
        try:
            X.insert(X.shape[1], line, percentage_return)
        except ValueError as e:
            print (line+" has %d lines of data, while "+i_list[i-1]+\
                   " has %d lines of data, cannot proceed calculation.") %(close_price.shape[0], X.shape[0])
            exit (-1)
        # using Open column to calculate expected price
        #miu.append(mean)
    # formula to calculate efficient portfolio
    sigma_x = X.cov()
    print sigma_x
    ones = [1]*len(i_list)
    x = np.dot(np.linalg.inv(sigma_x), ones)
    w = x/sum(x)
    print w

    with open(o_file, 'w+') as f:
        f.write("ticker,weight\n")
        for i in range(len(i_list)):
            f.write(i_list[i]+','+str(w[i]*100)+'%\n')
        f.close()


def test():
    line = 'IBM'
    aapl_data = Quandl.get("WIKI/"+line, trim_start="2014-10-25", trim_end="2015-10-24", authtoken=QUANDL_KEY)

    df = pd.DataFrame()
    df.insert(df.shape[1],line, df['Open'], allow_duplicates=True)
    print df
    return
    #print sum(df['y'])/float(df.shape[0])
    miu = [1,2,3]
    cov = df.cov()
    x = np.dot(np.linalg.inv(cov), miu)
    w = x/sum(x)
    print x
    print w



if __name__ == "__main__":
    main()
    #test()






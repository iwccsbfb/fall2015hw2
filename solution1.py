import sys
import datetime
import string
from bond import bond
from bond import yield_curve
import csv

def get_parameters():
    try:
        i = 1
        date = i_file = yc_file = o_file = ""
        while i < len(sys.argv):
            arg = sys.argv[i]
            if arg[0] == '-':
                remaining_arg = arg[1:]
                if remaining_arg == 'd':
                    i = i+1
                    date = sys.argv[i]
                elif remaining_arg == 'i':
                    i = i+1
                    i_file = sys.argv[i]
                elif remaining_arg == 'y':
                    i = i+1
                    yc_file = sys.argv[i]
                elif remaining_arg == 'o':
                    i = i+1
                    o_file = sys.argv[i]
                else:
                    raise Exception("invalid argument")
            else:
                raise Exception("invalid argument")
            i = i+1

        #check if [date, i_file, yc_file, o_file] is legal?
        if(date == "" or  i_file == "" or yc_file == "" or o_file == ""):
            raise Exception("invalid argument")
        start_date = datetime.date( int(date[0:4]), int(date[4:6]), int(date[6:8]) )
        with open(i_file, 'r') as f:
            i_file_data = f.read()
        with open(yc_file, 'r') as f:
            yc_file_data = f.read()
    except IOError as e:
        print e
        exit(-1)
    #catch all other kinds of exceptions
    except:  #except (Exception, ValueError, IndexError, IOError) as e:
        e = sys.exc_info()[0]
        print e
        print "usage: python solution1.py -d YYYYMMDD -i <input_file.csv> -y <input_yield_curve.csv> -o <output.csv>"
        exit(-1)
    return [start_date, i_file_data, yc_file_data, o_file]

def main():
    [today, i_file, yc_file, o_file] = get_parameters()
    yc = yield_curve(yc_file)
    i_lines = i_file.strip().split('\n')

    f = open(o_file, "w+")
    f.write("id,price,paryield,duration,convexity\n")
    for i in range(1, len(i_lines)):
        line = i_lines[i].split(',')
        tmp_bond = bond(line[0], line[1], line[2], line[3], line[4], line[5], today)
        tmp_bond.set_yield_curve(yc)
        f.write(tmp_bond.get_parameters())
    f.close()


if __name__ == "__main__":
    main()